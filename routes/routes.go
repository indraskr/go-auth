package routes

import (
	"github.com/dni9/go-auth/controllers"
	"github.com/gofiber/fiber/v2"
)

func Setup(app *fiber.App) {
	api := app.Group("/api")

	api.Post("/register", controllers.Register)
	api.Post("/login", controllers.Login)
	api.Get("/me", controllers.Me)
	api.Post("/logout", controllers.Logout)
}
